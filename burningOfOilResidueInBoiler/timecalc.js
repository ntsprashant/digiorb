					function zeroPutter(num)
					{
						if(num<10)
						{
							num='0'+num;
						}
						return num;
					}
					function timeToSeconds(time1,time2,dayDifference)
					{						
						var hour=time1.charAt(0)+time1.charAt(1);
						var min=time1.charAt(3)+time1.charAt(4);
						var second1=(hour*3600)+(min*60);
						var hour=time2.charAt(0)+time2.charAt(1);
						var min=time2.charAt(3)+time2.charAt(4);
						var second2=(hour*3600)+(min*60);
						var difference=second2-second1;
						var timeDiffrence;
						if(dayDifference==0)
						{
                            if(second2>=second1)
							{
                            timeDiffrence=zeroPutter(parseInt(difference/3600))+":"+zeroPutter(parseInt(difference%3600)/60);	
                            }
                            else{
                                timeDiffrence='00:00';
                                alert("Stop time should be Greater than start time");
                            }
						} 
						if(dayDifference>0)
						{						
							if(second2>=second1)
							{
							difference=dayDifference+difference;							
							}
							else{
								difference=(dayDifference-(24*3600))+((24*3600)-second1)+second2;
							}

							timeDiffrence=zeroPutter(parseInt(difference/3600))+":"+zeroPutter(parseInt(difference%3600)/60);						
						}
						if(dayDifference<0)
						{
                            timeDiffrence='00:00'
                            alert("Stop time should be Greater than start time");
						}
						document.getElementById('timeDifference').value=timeDiffrence;
						angular.element(jQuery("#timeDifference")).triggerHandler('input');
						
					}
					function timeCalculation()
					{		
						var time1=$("#datetimepicker1").find("input").val();
						var time2=$("#datetimepicker2").find("input").val();					
						var date1=new Date($("#date1").val());
						var date2=new Date($("#date2").val());
						var dayDifference=(date2-date1)/(1000);
						////alert("this is actual Day Difference "+dayDifference);
						timeToSeconds(time1,time2,dayDifference);	
					}